#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("vhOA00kbP7Bi/ZBFDUxxzlFwAPqQpM+Eww4eIAWvJtarY+Mxxus3HK0xxMuvGotWbmX03A/T/AXDwpTQgKFq9p4HAfHL5tunHqh+X5NcK1Km65WgNkyGRfgxUw/yvuznu6mBr7BBMMl2Yx2uHvbPFXZ62/X1LWDo+Xp0e0v5enF5+Xp6e/yX4Nd9r9ESbBQD6hY/JpNitY/Ek9WqSRUbAVUJLlWLmFbLD91rU7RU+17lQc0xZudZfoTxMrdvY6dhJYnKTmXlVq+egO83SCfp+R349n9M0yMcVUg5B2DmaRc4g5daZO18y8KlsD3VV9LtxXBVVPSjOUNJFQQmdNg5Hp9xb+BL+XpZS3Z9clH9M/2Mdnp6en57eGaCr42yJ3OeBHl4ent6");
        private static int[] order = new int[] { 9,13,10,11,6,12,13,8,10,11,10,12,13,13,14 };
        private static int key = 123;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
