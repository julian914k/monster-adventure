#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("Wbb/QQfVWScZObLCe1hV8R7+IdKwkYaD1YSKk4rB8PDs5aDJ7uOusb2m56AKs+p3jQJPXmsjr3nT6tvk2SeFifyXwNaRnvRTNwuju8cjVe8xsNhs2oSyDOgzD51e5fN/597lPP/BKBh5UUrmHKTrkVAjO2SbqkOflrCUhoPVhIOTjcHw8OzloNLv7/Tw7OWg0u/v9KDDwbCel42wtrC0sojesAKBkYaD1Z2ghAKBiLACgYSw+aDh8/P17eXzoOHj4+Xw9OHu4+WGsI+Gg9Wdk4GBf4SFsIOBgX+wnbAChDuwAoMjIIOCgYKCgYKwjYaJ7OWgye7jrrGmsKSGg9WEi5OdwfCzttqw4rGLsImGg9WEhpOC1dOxk4aD1Z2OhJaElKtQ6ccU9ol+dOsNAJSrUOnHFPaJfnTrDa7AJnfHzf+fEVuex9BrhW3e+QSta7Yi18zVbOcPiDSgd0ssrKDv8Da/gbAMN8NPqgbIBneNgYGFhYCw4rGLsImGg9ULmQleecvsdYcrorCCaJi+eNCJU/f3ruHw8OzlruPv7a/h8PDs5ePhtbKxtLCzttqXjbO1sLKwubKxtLC2Gcyt+DdtDBtcc/cbcvZS97DPQYds/bkDC9OgU7hEMT8az4rrf6t8jYaJqgbIBneNgYGFhYCDAoGBgNz06ebp4+H05aDi+aDh7vmg8OHy9KDh7uSg4+Xy9Onm6ePh9Onv7qDw0uXs6eHu4+Wg7+6g9Ojp86Dj5fI3mz0TwqSSqkePnTbNHN7jSMsAl67AJnfHzf+I3rCfhoPVnaOEmLCWNbotdI+OgBKLMaGWrvRVvI1b4pYrI/ESx9PVQS+vwTN4e2PwTWYjzAKBgIaJqgbIBnfj5IWBsAFysKqGr7ABQ4aIq4aBhYWHgoKwATaaATP06O/y6fT5sZawlIaD1YSDk43B8OLs5aDz9OHu5OHy5KD05fLt86DhiKuGgYWFh4KBlp7o9PTw87qvr/c+dPMbblLkj0v5z7RYIr55+H/rSI8dvXOryaiaSH5ONTmOWd6cVku98uHj9Onj5aDz9OH05e3l7vTzrrAP8wHgRpvbia8SMnjEyHDguB6VdYWAgwKBj4CwAoGKggKBgYBkESmJoO/moPTo5aD06OXuoOHw8Ozp4+HF/p/M69AWwQlE9OKLkAPBB7MKAfqwAoH2sI6Gg9Wdj4GBf4SEg4KBpGJrUTfwX4/FYadKce34bWc1l5coXP6itUqlVVmPVutUIqSjkXchLKyg4+Xy9Onm6ePh9OWg8O/s6eP5QOOz93e6h6zWa1qPoY5aOvOZzzUVHvqMJMcL21SWt7NLRI/NTpTpUclY9h+zlOUh9xRJrYKDgYCBIwKB8OzloMPl8vTp5unj4fTp7+6gwfWEhpOC1dOxk7CRhoPVhIqTisHw8J8FAwWbGb3Ht3IpG8AOrFQxEJJY6ebp4+H06e/uoMH19Ojv8un0+bGgw8GwAoGisI2GiaoGyAZ3jYGBgUmZ8nXdjlX/3xtypYM61Q/N3Y1xprCkhoPVhIuTncHw8OzloMPl8vTu5KDj7+7k6fTp7+7zoO/moPXz5eS1o5XLldmdMxR3dhweT9A6QdjQ0CoKVVpkfFCJh7cw9fWh");
        private static int[] order = new int[] { 58,9,56,52,26,17,12,12,33,40,29,58,52,33,30,32,32,21,40,32,36,52,48,51,47,39,45,38,59,31,51,47,34,37,52,42,42,50,48,46,55,51,51,46,59,53,54,59,57,55,57,53,56,55,58,57,56,59,58,59,60 };
        private static int key = 128;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
