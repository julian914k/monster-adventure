﻿using UnityEngine;
using System.Collections;
using CnControls;

[RequireComponent(typeof(Character))]
public class ColleagueAI : MonoBehaviour
{
    public Character Target;
    public float JumpDelay = 0.5f;
    public float DistanceToTarget = 1;
    public float DistanceToTargetLimit = 10;
    
    private CharacterInput characterInput;
    private Character character;
    private Vector2 axis;
    private bool allowJump;

    public CharacterInput CharacterInput {get; private set;}
    public Character Character {get; private set;}


    void Start()
    {
        this.character = this.GetComponent<Character>();
        this.axis = new Vector2();
        this.characterInput = this.GetComponent<CharacterInput>();

        Character = character;
        CharacterInput = characterInput;
        
        int playerLayerMaskNumber = LayerMask.NameToLayer("Player");
        Physics2D.IgnoreLayerCollision(playerLayerMaskNumber, playerLayerMaskNumber);
    }

    void FixedUpdate()
    {
        if(Target == null)
            return;

        float currnetDistanceToTarget = Mathf.Abs(Target.transform.position.x - transform.position.x);
        if(currnetDistanceToTarget > DistanceToTarget)
        {
            axis.x = Target.transform.position.x - transform.position.x < 0 ? -1 : 1;
        }
        else
            axis.x = 0;
        axis.y = 0;

        this.character.Move(this.axis, true);
    }

    void Update()
    {
        if(Target == null)
            return;
            
        Character.Action actionFlags = 0;

        actionFlags |= (CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.RunModified : 0);
        actionFlags |= (CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.Crouch : 0);
        if(CnInputManager.GetButtonDown("Jump"))
            StartCoroutine(JumpAfter());
        actionFlags |= (allowJump ? Character.Action.Jump : 0);
        actionFlags |= (CnInputManager.GetButtonDown("Jump") && CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.JumpDown : 0);

        this.character.Perform(actionFlags);

        float currentDistanceToTarget = Mathf.Abs(Target.transform.position.x - transform.position.x);
        if(currentDistanceToTarget > DistanceToTargetLimit)
        {
            Respawn();
        }

        // 테스트용
        if(Input.GetKeyDown(KeyCode.F1))
            Respawn();
    }

    void Respawn()
    {
        transform.position = new Vector3(Target.transform.position.x + (DistanceToTarget * (int)Target.CurrentDirection * -1), Target.transform.position.y + 10);
    }

    IEnumerator JumpAfter()
    {
        yield return new WaitForSeconds(JumpDelay);

        allowJump = true;

        yield return null;
        
        allowJump = false;
    }
}
