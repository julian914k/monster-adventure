﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterReplaceManager: MonoBehaviour 
{
    class ColleagueAIInfo
    {
        public float JumpDelay = 0.5f;
        public float DistanceToTarget = 1;
        public float DistanceToTargetLimit = 10;
    }

	public ColleagueAI[] Colleagues;
    public float CoolTime = 3f;

    private ColleagueAIInfo[] initialSetting;
    private int currentIndex = 0;

    void Start()
    {
        initialSetting = new ColleagueAIInfo[Colleagues.Length];
        for(int i = 0;i < Colleagues.Length; ++i)
        {
            initialSetting[i] = new ColleagueAIInfo();
            initialSetting[i].JumpDelay = Colleagues[i].JumpDelay;
            initialSetting[i].DistanceToTarget = Colleagues[i].DistanceToTarget;
            initialSetting[i].DistanceToTargetLimit = Colleagues[i].DistanceToTargetLimit;
        }
        SetPlayer(0);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F2))
        {
            Replace();
        }
    }

    void Replace()
    {
        Vector3[] positions = new Vector3[Colleagues.Length];
        for(int i = 0;i < Colleagues.Length; ++i)
        {
            positions[i] = Colleagues[(currentIndex + i) % Colleagues.Length].transform.position;
        }

        SetPlayer((++currentIndex) % Colleagues.Length);
        for(int i = 0;i < Colleagues.Length; ++i)
        {
            SetInfo(Colleagues[(currentIndex + i) % Colleagues.Length], initialSetting[i]);
            Colleagues[(currentIndex + i) % Colleagues.Length].transform.position = positions[i];
        }
    }

    void SetPlayer(int index)
    {
        ColleagueAI player = Colleagues[index];
        player.CharacterInput.enabled = true;
        player.enabled = false;
        foreach(ColleagueAI colleague in Colleagues)
        {
            if(colleague == player)
                continue;
            colleague.CharacterInput.enabled = false;
            colleague.enabled = true;
            colleague.Target = player.Character;
        }
    }

    void SetInfo(ColleagueAI colleague, ColleagueAIInfo info)
    {
        colleague.JumpDelay = info.JumpDelay;
        colleague.DistanceToTarget = info.DistanceToTarget;
        colleague.DistanceToTargetLimit = info.DistanceToTargetLimit;
    }
}
