﻿using UnityEngine;
using System.Collections;

public class ThrownProjectile : HorizontalProjectile
{
    [Header("Thrown")]
    public bool IsMainItem = true;
    public float StartRotation = 0f;
    public float EndRotation = 0f;

    private bool isEndRotationSet;

    protected override void Start()
    {
        base.Start();

        Sprite weaponSprite = this.GetComponentInChildren<SpriteRenderer>().sprite;
        BoxCollider2D weaponBox = this.GetComponentInChildren<BoxCollider2D>();
        if (this.Owner != null)
        {
            SpriteRenderer[] parts = this.Owner.GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].name == (this.IsMainItem ? "MainItem" : "OffItem"))
                {
                    weaponSprite = parts[i].sprite;
                    weaponBox = parts[i].gameObject.GetComponent<BoxCollider2D>();
                    break;
                }
            }
        }

        if (weaponSprite != null)
        {
            SpriteRenderer sr = this.GetComponentInChildren<SpriteRenderer>();
            if (sr != null)
            {
                sr.sprite = weaponSprite;
            }
        }

        if (weaponBox != null)
        {
            BoxCollider2D bc = this.GetComponentInChildren<BoxCollider2D>();
            if (bc != null)
            {
                bc.offset = weaponBox.offset;
                bc.size = weaponBox.size;
            }

            WeaponProjectile.IgnoreOwnerCollisions(this, this.Owner);

            this.RenderTransform.localPosition = -weaponBox.offset;
            this.RenderTransform.RotateAround(this.transform.position, Vector3.forward, this.StartRotation * this.DirectionX);
        }
    }

    protected override void OnCollisionEnter2D(Collision2D c)
    {
        base.OnCollisionEnter2D(c);

        if (this.isStopped && !this.isEndRotationSet)
        {
            this.isEndRotationSet = true;

            if (!this.ShouldFallWhenCollided)
            {
                float rotation = this.EndRotation - this.RenderTransform.localEulerAngles.z;
                this.RenderTransform.RotateAround(this.transform.position + this.RotationPoint, Vector3.forward, rotation * this.DirectionX);
            }
        }
    }
}
