﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class TeamAI : MonoBehaviour
{
    public Character TargetCharacter;
    public float FollowRange;
    public Vector2 RespawnRange;
    public float JumpDelayTime;
    private Character character;
    private Vector2 axis;
    private Vector2 targetDistance;
    private bool isJumpEnabled = false;

    void Start()
    {
        character = GetComponent<Character>();
    }
    void FixedUpdate()
    {
        if (TargetCharacter == null)
            return;

        targetDistance.x = TargetCharacter.transform.position.x - transform.position.x;
        targetDistance.y = TargetCharacter.transform.position.y - transform.position.y;

        if (Mathf.Abs(targetDistance.x) > FollowRange)
            axis.x = TargetCharacter.transform.position.x - transform.position.x < 0 ? -1 : 1;
        else
            axis.x = 0;

        axis.y = 0;

        character.Move(axis, true);
    }
    void Update()
    {
        if (TargetCharacter == null)
            return;

        if (targetDistance.x > RespawnRange.x || targetDistance.y > RespawnRange.y)
        {
            Respawn();
            return;
        }

        Character.Action actionFlags = 0;

        actionFlags |= (CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.RunModified : 0);
        actionFlags |= (CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.Crouch : 0);
        if (CnInputManager.GetButtonDown("Jump"))
            StartCoroutine(Jump());
        actionFlags |= (isJumpEnabled ? Character.Action.Jump : 0);
        actionFlags |= (CnInputManager.GetButtonDown("Jump") && CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.JumpDown : 0);

        this.character.Perform(actionFlags);
    }

    IEnumerator Jump()
    {
        yield return new WaitForSeconds(JumpDelayTime);

        isJumpEnabled = true;

        yield return null;

        isJumpEnabled = false;
    }

    private void Respawn()
    {
        transform.position = new Vector3(TargetCharacter.transform.position.x + (FollowRange * (int)TargetCharacter.CurrentDirection * -1), TargetCharacter.transform.position.y + 5);
    }
}
