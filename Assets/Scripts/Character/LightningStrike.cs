﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightningStrike : WeaponEffect
{
    [Header("Strike")]
    public float StrikeHeight = 6;
    public int ControlPoints = 2;
    public float PatternChangeInterval = 0.05f;
    public float StrikeWidth = 1f;

    [Header("Effect")]
    public bool ChangeBackgroundColor = true;
    public Color BackgroundColor = Color.black;

    private LineRenderer strikeLine;
    private float changeTime;

    protected override void Start()
    {
        base.Start();

        this.strikeLine = GetComponent<LineRenderer>();

        this.strikeLine.enabled = false;

        if (this.ChangeBackgroundColor)
        {
            WeaponEffect.SetBackgroundColor(this.BackgroundColor);
        }
    }

    protected override void Update()
    {
        this.changeTime -= Time.deltaTime;

        if (this.changeTime <= 0)
        {
            this.UpdateStrikePattern();
            this.changeTime = this.PatternChangeInterval;
        }
    }

    public override void Stop()
    {
        base.Stop();

        this.strikeLine.enabled = false;
    }

    private void UpdateStrikePattern()
    {
        Vector3[] points = new Vector3[this.ControlPoints + 2];
        points[0] = this.target.position;

        for (int i = 1; i < points.Length; i++)
        {
            float x = this.target.position.x + Random.Range(-this.StrikeWidth, this.StrikeWidth);
            float y = ((float)i / (this.ControlPoints + 1)) * StrikeHeight;
            points[i] = new Vector3(x, this.target.position.y + y, 0);
        }

        this.strikeLine.positionCount = points.Length;
        for (int i = 0; i < points.Length; i++)
        {
            this.strikeLine.SetPosition(i, points[i]);
        }

        this.strikeLine.enabled = true;
    }
}