﻿using UnityEngine;
using System.Collections;

public abstract class WeaponProjectile : MonoBehaviour
{
    [Header("Weapon")]
    public int Damage = 1;
    public float LifeTime = 3;
    public bool ShouldCollideWithGround;
    public bool ShouldFallWhenCollided = false;
    public LayerMask OverrideGroundLayer;
    public ParticleSystem CollideEmitter;
    public bool ShouldFlipDirection = true;

    protected Character Owner { get; private set; }
    protected int DirectionX { get; private set; }
    protected Animator animatorObject;
    protected bool isStopped;

    public static WeaponProjectile Create(WeaponProjectile instance, Character owner, Transform launchPoint, int directionX)
    {
        WeaponProjectile projectile = GameObject.Instantiate<WeaponProjectile>(instance);
        projectile.Owner = owner;

        Vector2 position = launchPoint.position;
        projectile.transform.position = position;
        projectile.DirectionX = directionX;

        WeaponProjectile.IgnoreOwnerCollisions(projectile, owner);

        if (projectile.ShouldFlipDirection && directionX < 0)
        {
            Vector3 rotation = projectile.transform.localRotation.eulerAngles;
            rotation.y -= 180;
            projectile.transform.localEulerAngles = rotation;
        }

        return projectile;
    }

    protected virtual void Start()
    {
        this.animatorObject = this.GetComponentInChildren<Animator>();

        StartCoroutine(this.DestroyAfter(this.LifeTime));
    }

    protected virtual void Update()
    {
    }

    protected virtual void OnCollisionEnter2D(Collision2D c)
    {
        if (this.isStopped)
        {
            return;
        }

        if (Owner.tag == "Monster" && c.transform.tag == "Monster")
            return;

        LayerMask ground = (this.OverrideGroundLayer == 0 && this.Owner != null ? this.Owner.GroundLayer : this.OverrideGroundLayer);
        bool isOnGround = (ground & (1 << c.gameObject.layer)) != 0;

        if (isOnGround)
        {
            if (this.ShouldCollideWithGround)
            {
                if (this.animatorObject != null)
                {
                    this.animatorObject.Play("Hit");
                }

                if (this.ShouldFallWhenCollided)
                {
                    SliderJoint2D joint = this.GetComponent<SliderJoint2D>();
                    if (joint != null)
                    {
                        joint.enabled = false;
                    }
                }
                else
                {
                    Rigidbody2D body = this.GetComponentInChildren<Rigidbody2D>();
                    if (body != null)
                    {
                        body.isKinematic = true;
                    }

                    Collider2D[] colliders = this.GetComponentsInChildren<Collider2D>();
                    for (int i = 0; i < colliders.Length; i++)
                    {
                        colliders[i].enabled = false;
                    }
                }

                if (this.CollideEmitter != null)
                {
                    this.CollideEmitter.Play();
                }

                this.isStopped = true;
            }
            else
            {
                Collider2D[] projectileColliders = this.GetComponentsInChildren<Collider2D>();
                for (int i = 0; i < projectileColliders.Length; i++)
                {
                    Physics2D.IgnoreCollision(projectileColliders[i], c.collider);
                }
            }
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }

        Character character = c.transform.GetComponent<Character>();
        if (character != null)
        {
            character.ApplyDamage(this.Damage);

            if (c.transform.tag == "Monster")
            {
                character.HPBarManager.UpdateHPBar();
            }
        }
    }

    protected static void IgnoreOwnerCollisions(WeaponProjectile projectile, Character owner)
    {
        if (owner != null)
        {
            Collider2D[] colliders = owner.GetComponentsInChildren<Collider2D>();
            Collider2D[] projectileColliders = projectile.GetComponentsInChildren<Collider2D>();
            for (int i = 0; i < colliders.Length; i++)
            {
                for (int j = 0; j < projectileColliders.Length; j++)
                {
                    Physics2D.IgnoreCollision(colliders[i], projectileColliders[j]);
                }
            }
        }
    }

    protected IEnumerator DestroyAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        GameObject.Destroy(this.gameObject);
    }
}
