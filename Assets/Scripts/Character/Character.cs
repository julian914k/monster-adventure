﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class Character : MonoBehaviour
{
    [Header("Character")]
    public Transform GroundChecker;
    public LayerMask GroundLayer;
    public float RunSpeed = 250;
    [Range(-1, 1)]
    public float RunModifierFactor = -0.75f;
    [Range(-1, 1)]
    public float BlockingMoveFactor = -0.75f;
    public bool AllowDoubleJump = true;
    public bool AllowAirControl = true;
    public float JumpPower = 550;
    public bool AllowWallJump = true;
    public Transform WallCheckerFront;
    public Transform WallCheckerBack;
    public LayerMask WallLayer;
    [Range(-1, 1)]
    public float WallSlideFactor = -0.25f;
    public float WallJumpHorizontalPower = 100;
    public bool AllowJumpDownPlatforms = true;
    public float JumpDownTimeout = 1;
    public bool UseLadders = true;
    public bool UseWater = true;
    [Range(-1, 1)]
    public float WaterMoveFactor = -0.75f;
    public bool UseIce = true;
    public float IceFriction = 1f;
    public bool IgnoreAnimationStates = false;
    public int MaxHealth = 3;
    public int MaxProjectileCount = 3;
    public float GravityScale = 3;
    public bool IsZombified = false;
    public ParticleSystem SplashEmitter;
    public ParticleSystem BubbleEmitter;
    public ParticleSystem DustEmitter;
    public float DustCloudThreshold = -10;

    [Header("Weapon")]
    public WeaponType EquippedWeaponType;
    public bool IsBlockEnabled = false;
    public Transform AttackPoint;
    public Transform LaunchPoint;
    public WeaponProjectile CastProjectile;
    public WeaponProjectile ThrowMainProjectile;
    public WeaponProjectile ThrowOffProjectile;
    public Transform EffectPoint;
    public WeaponEffect Effect;
    public float MeleeAttackCoolTime = 1.0f;
    public float RangedAttackCoolTime = 2.0f;
    [Header("Player")]
    public GameObject RIPStone;

    //public Action CurrentAction {get; set;}
    public int CurrentHealth { get; set; }
    public int CurrentProjectileCount { get; set; }
    public bool IsDead { get { return this.CurrentHealth <= 0; } }
    public bool IsPlayer { get; private set; }
    public Direction CurrentDirection { get; set; }
    public HPBarManager HPBarManager { get; private set; }
    public float ModifiedSpeed
    {
        get
        {
            return this.RunSpeed * this.GetMultiplier(this.RunModifierFactor);
        }
    }
    public bool IsAttacking
    {
        get
        {
            AnimatorStateInfo state = this.animator.GetCurrentAnimatorStateInfo(3);
            return state.IsName("Attack") || state.IsName("Quick Attack");
        }
    }

    public enum WeaponType
    {
        None = 0,
        Staff = 1,
        Sword = 2,
        Bow = 3,
        Gun = 4
    }

    public enum Direction
    {
        Left = -1,
        Right = 1
    }

    [Flags]
    public enum Action
    {
        Jump = 1,
        RunModified = 2,
        QuickAttack = 4,
        Attack = 8,
        Cast = 16,
        ThrowOff = 32,
        ThromMain = 64,
        Consume = 128,
        Block = 256,
        Hurt = 512,
        JumpDown = 1024,
        Crouch = 2048
    }

    private Animator animator;
    private Rigidbody2D body2D;
    [SerializeField]
    private bool isGrounded = true;
    private bool isOnWall = false;
    private bool isOnWallFront = false;
    private bool isOnLadder = false;
    private bool isInWater = false;
    private bool isOnIce = false;
    private bool isBlocking = false;
    private bool isCrouching = false;
    private bool isJumpPressed;
    private bool isJumpingDown;
    private bool isReadyForDust;
    private int jumpCount = 0;
    private bool isRunningNormal = true;
    private float groundRadius = 0.1f;
    private float wallDecayX = 0.006f;
    private float wallJumpX = 0;
    private WeaponEffect activeEffect;
    private Direction startDirection = Direction.Right;

    public Rigidbody2D GetRigidbody2D()
    {
        return body2D;
    }

    public static Character Create(Character instance, Direction startDirection, Vector3 position)
    {
        Character c = GameObject.Instantiate<Character>(instance);
        c.transform.position = position;
        c.startDirection = startDirection;
        return c;
    }

    void Awake()
    {
        this.body2D = this.GetComponent<Rigidbody2D>();
        this.animator = this.GetComponent<Animator>();
        HPBarManager = GetComponentInChildren<HPBarManager>();

        if (gameObject.tag == "Player")
        {
            IsPlayer = true;
        }
        else
        {
            IsPlayer = false;
        }

        if (this.body2D != null)
        {
            this.body2D.gravityScale = this.GravityScale;
        }

        this.CurrentHealth = this.MaxHealth;
        CurrentProjectileCount = MaxProjectileCount;
        this.ApplyDamage(0);
        this.body2D.centerOfMass = new Vector2(0f, 0.4f);
        if (this.startDirection != Direction.Right)
        {
            this.ChangeDirection(this.startDirection);
        }
        else
        {
            this.CurrentDirection = this.startDirection;
        }

        if (this.BubbleEmitter != null && this.BubbleEmitter.isPlaying)
        {
            this.BubbleEmitter.Stop();
            this.BubbleEmitter.gameObject.SetActive(false);
        }

        if ((this.GroundLayer & (1 << this.gameObject.layer)) != 0)
        {
            Debug.LogWarningFormat(this, "The character has its GroundLayer set incorrectly.\r\nThe GroundLayer matches the Character's main Layer, so it will not jump/fall correctly\r\nPlease update either the GroundLayer or the Layer of the character.");
        }

        this.isGrounded = this.CheckGround();
        animator.SetBool("IsGrounded", this.isGrounded);
    }

    void FixedUpdate()
    {
        this.isGrounded = this.CheckGround();
        animator.SetBool("IsGrounded", this.isGrounded);

        bool isOnWallFront = false;
        bool isOnWallBack = false;
        if (this.AllowWallJump && !this.isGrounded && this.body2D.velocity.y <= 0)
        {
            isOnWallFront = Physics2D.OverlapCircle(WallCheckerFront.position, this.groundRadius, this.WallLayer);
            isOnWallBack = Physics2D.OverlapCircle(WallCheckerBack.position, this.groundRadius, this.WallLayer);
        }

        this.isOnWall = (isOnWallFront || isOnWallBack);
        this.isOnWallFront = (this.isOnWall && isOnWallFront);
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if ((this.GroundLayer.value & 1 << c.gameObject.layer) != 0 &&
            c.contacts[0].normal.y > 0.8f)
        {
            if (this.isReadyForDust || this.body2D.velocity.y < this.DustCloudThreshold)
            {
                this.CreateDustCloud(c.contacts[0].point);
            }
            this.isReadyForDust = false;
        }

        if (IsTrigger(c.gameObject, "spike"))
        {
            ApplyDamage(1);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (this.UseWater && this.IsTrigger(other.gameObject, "water"))
        {
            Vector2 pos = this.GroundChecker.position;
            pos.y = other.bounds.center.y + other.bounds.extents.y;
            this.CreateSplash(pos);

            if (this.BubbleEmitter != null && !this.BubbleEmitter.isPlaying)
            {
                this.BubbleEmitter.gameObject.SetActive(true);
                this.BubbleEmitter.Play();
            }
        }

        if (IsTrigger(other.gameObject, "lava"))
        {
            ApplyDamage(100);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (this.UseLadders && this.IsTrigger(other.gameObject, "ladder"))
        {
            this.body2D.isKinematic = true;
            this.isOnLadder = true;
            this.animator.SetBool("IsOnLadder", this.isOnLadder);
        }

        if (this.UseWater && this.IsTrigger(other.gameObject, "water"))
        {
            this.isInWater = true;
            this.animator.SetBool("IsInWater", this.isInWater);
        }

        if (this.UseIce && this.IsTrigger(other.gameObject, "ice"))
        {
            this.isOnIce = true;
            this.animator.SetBool("IsOnIce", this.isOnIce);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (this.UseLadders && this.IsTrigger(other.gameObject, "ladder"))
        {
            this.body2D.isKinematic = false;
            this.isOnLadder = false;
            this.animator.SetBool("IsOnLadder", this.isOnLadder);
        }

        if (this.UseWater && this.IsTrigger(other.gameObject, "water"))
        {
            this.isInWater = false;
            this.animator.SetBool("IsInWater", this.isInWater);

            if (this.BubbleEmitter != null && this.BubbleEmitter.isPlaying)
            {
                this.BubbleEmitter.Stop();
                this.BubbleEmitter.gameObject.SetActive(false);
            }
        }

        if (this.UseIce && this.IsTrigger(other.gameObject, "ice"))
        {
            this.isOnIce = false;
            this.animator.SetBool("IsOnIce", this.isOnIce);
        }
    }

    public void Move(Vector2 axis, bool isHorizontalStillPressed)
    {
        if (this.IsDead)
        {
            this.body2D.velocity = new Vector2(0, this.body2D.velocity.y);
            return;
        }

        if (this.IgnoreAnimationStates)
        {
            this.isGrounded = true;
            this.animator.SetBool("IsGrounded", this.isGrounded);
            return;
        }

        if (this.isGrounded || this.AllowAirControl)
        {
            float horizontal = axis.x;

            if (this.AllowWallJump && !this.isGrounded && !Mathf.Approximately(this.wallJumpX, 0))
            {
                if (!isHorizontalStillPressed)
                {
                    this.wallJumpX = Mathf.Lerp(this.wallJumpX, 0, Time.deltaTime);
                    horizontal = this.wallJumpX;
                }
                else
                {
                    this.wallJumpX = 0;
                }
            }

            float speed = (this.isRunningNormal ? this.RunSpeed : this.ModifiedSpeed);
            Vector2 newVelocity = new Vector2(horizontal * speed * Time.deltaTime, this.body2D.velocity.y);

            if (this.isOnIce)
            {
                newVelocity.x = Mathf.Lerp(this.body2D.velocity.x, newVelocity.x, this.IceFriction * Time.deltaTime);
            }

            this.body2D.velocity = newVelocity;
        }

        if (this.isJumpPressed)
        {
            float xPower = 0;
            if (this.AllowWallJump && this.isOnWall)
            {
                xPower = this.WallJumpHorizontalPower * (int)this.CurrentDirection;
                this.wallJumpX = xPower * this.wallDecayX;

                this.CreateDustCloud(this.GroundChecker.position);
            }
            else
            {
                this.wallJumpX = 0;
            }

            this.body2D.velocity = new Vector2(this.body2D.velocity.x, 0);
            this.body2D.AddForce(new Vector2(xPower, this.JumpPower));
            this.isJumpPressed = false;
            this.isReadyForDust = true;

            if (this.jumpCount == 2)
            {
                this.animator.Play("Roll", LayerMask.NameToLayer("FX"));
            }
        }
        else if (this.isOnLadder)
        {
            float vertical = axis.y;
            this.body2D.velocity = new Vector2(this.body2D.velocity.x, vertical * this.RunSpeed * Time.deltaTime);
        }
        else if (this.isOnWall && !this.isGrounded)
        {
            if (this.body2D.velocity.y < 0 &&
                (this.body2D.velocity.x >= 0 && this.CurrentDirection < 0 ||
                 this.body2D.velocity.x <= 0 && this.CurrentDirection > 0))
            {
                this.body2D.velocity = new Vector2(this.body2D.velocity.x, this.body2D.velocity.y * this.GetMultiplier(this.WallSlideFactor));
            }
        }
        else if (this.isBlocking)
        {
            this.body2D.velocity = new Vector2(this.body2D.velocity.x * this.GetMultiplier(this.BlockingMoveFactor), this.body2D.velocity.y);
        }

        if (this.isInWater)
        {
            float waterFactor = this.GetMultiplier(this.WaterMoveFactor);
            this.body2D.velocity = new Vector2(this.body2D.velocity.x * waterFactor, this.body2D.velocity.y);
        }

        this.animator.SetBool("IsGrounded", this.isGrounded);
        this.animator.SetBool("IsOnWall", this.isOnWall);
        this.animator.SetInteger("WeaponType", (int)this.EquippedWeaponType);
        this.animator.SetBool("IsZombified", this.IsZombified);
        this.animator.SetFloat("AbsY", Mathf.Abs(this.body2D.velocity.y));
        this.animator.SetFloat("VelocityY", this.body2D.velocity.y);
        this.animator.SetFloat("VelocityX", Mathf.Abs(this.body2D.velocity.x));
        this.animator.SetBool("HasMoveInput", isHorizontalStillPressed);
        this.animator.SetBool("IsCrouching", this.isCrouching && this.isGrounded);

        if (this.isOnWall)
        {
            if (this.isOnWallFront && !this.isOnLadder)
            {
                this.ChangeDirection(this.CurrentDirection == Direction.Left ? Direction.Right : Direction.Left);
            }
        }
        else if (this.body2D.velocity.x != 0)
        {
            this.ChangeDirection(this.body2D.velocity.x < 0 ? Direction.Left : Direction.Right);
        }
    }

    public void Perform(Action action)
    {
        if (this.IsDead)
        {
            return;
        }

        this.isBlocking = IsAction(action, Action.Block) && this.IsBlockEnabled;

        this.isCrouching = IsAction(action, Action.Crouch);

        this.isRunningNormal = !IsAction(action, Action.RunModified);

        if (this.isGrounded)
        {
            this.jumpCount = 0;
        }

        if (IsAction(action, Action.JumpDown))
        {
            Collider2D ground = this.CheckGround();
            if (ground != null)
            {
                PlatformEffector2D fx = ground.GetComponent<PlatformEffector2D>();
                if (fx != null && fx.useOneWay)
                {
                    ground.enabled = false;
                    action &= ~Action.Jump;

                    StartCoroutine(this.EnableAfter(this.JumpDownTimeout, ground));
                }
            }
        }

        if (IsAction(action, Action.Jump) && !this.isJumpPressed)
        {
            if (!this.isOnLadder)
            {
                if (this.isGrounded || (this.AllowWallJump && this.isOnWall))
                {
                    this.isJumpPressed = true;
                    this.jumpCount = 1;
                }
                else if (this.AllowDoubleJump && this.jumpCount <= 1)
                {
                    this.isJumpPressed = true;
                    this.jumpCount = 2;
                }
            }
        }
        else if (IsAction(action, Action.QuickAttack))
        {
            this.TriggerAction("TriggerQuickAttack");

            string targetLayer = null;
            if (transform.tag == "Player")
            {
                targetLayer = "Monster";
            }
            else if (transform.tag == "Monster")
            {
                targetLayer = "Player";
            }

            // Character Action이 공격일 때, OverlapCircleAll 을 이용해서, 피격 여부를 확인함.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(this.EffectPoint.position,
                0.4f, 1 << LayerMask.NameToLayer(targetLayer));

            foreach (Collider2D collider in colliders)
            {
                if (collider.tag == targetLayer)
                {
                    Character targetCharacter = collider.GetComponent<Character>();
                    targetCharacter.ApplyDamage(1);

                    if (collider.tag == "Monster")
                    {
                        targetCharacter.HPBarManager.UpdateHPBar();
                    }

                    return;
                }
            }

        }
        else if (IsAction(action, Action.Attack))
        {
            string targetLayer = null;
            if (transform.tag == "Player")
            {
                targetLayer = "Monster";
            }
            else if (transform.tag == "Monster")
            {
                targetLayer = "Player";
            }

            Collider2D[] colliders = Physics2D.OverlapCircleAll(this.EffectPoint.position,
                0.4f, 1 << LayerMask.NameToLayer(targetLayer));

            foreach (Collider2D collider in colliders)
            {
                if (collider.tag == targetLayer)
                {
                    Character targetCharacter = collider.GetComponent<Character>();
                    targetCharacter.ApplyDamage(1);
                    if (collider.tag == "Monster")
                    {
                        targetCharacter.HPBarManager.UpdateHPBar();
                    }
                    return;
                }
            }
        }
        else if (IsAction(action, Action.Cast))
        {
            this.TriggerAction("TriggerCast");

            CurrentProjectileCount = Mathf.Clamp(this.CurrentProjectileCount - 1, 0, this.MaxProjectileCount);

            if (gameObject.tag == "Player")
            {
                UpdateCurrentProjectileCountUI();
            }
        }
        else if (IsAction(action, Action.ThrowOff))
        {
            this.TriggerAction("TriggerThrowOff");
        }
        else if (IsAction(action, Action.ThromMain))
        {
            this.TriggerAction("TriggerThrowMain");
        }
        else if (IsAction(action, Action.Consume))
        {
            this.TriggerAction("TriggerConsume");
        }
        else if (this.isBlocking && !this.animator.GetBool("IsBlocking"))
        {
            this.TriggerAction("TriggerBlock");
        }
        else if (IsAction(action, Action.Hurt))
        {
        }

        if (!this.isBlocking)
        {
            this.animator.SetBool("IsBlocking", this.isBlocking);
        }
    }

    public bool ApplyDamage(int damage, float direction = 0)
    {
        if (!this.IsDead)
        {
            this.animator.SetFloat("LastHitDirection", direction * (int)this.CurrentDirection);

            // 피격시 생명력 주는 코드 부분
            this.CurrentHealth = Mathf.Clamp(this.CurrentHealth - damage, 0, this.MaxHealth);

            // 개발시 테스트용 모든 캐릭터 무적으로 만드는 코드
            //this.CurrentHealth = this.MaxHealth;

            this.animator.SetInteger("Health", this.CurrentHealth);

            if (gameObject.tag == "Player")
            {
                UpdateCurrentHealthUI();
            }

            if (damage != 0)
            {
                this.TriggerAction("TriggerHurt", false);
                this.animator.Play("Damage", 5);
            }

            if (this.CurrentHealth <= 0)
            {
                StartCoroutine(this.DestroyAfter(1, this.gameObject));
            }
        }
        return this.IsDead;
    }

    public void UpdateCurrentProjectileCountUI()
    {
        switch (CurrentProjectileCount)
        {
            case 3:
                GameObject.Find("ProjectileFull3").GetComponent<Image>().enabled = true;
                GameObject.Find("ProjectileFull2").GetComponent<Image>().enabled = true;
                GameObject.Find("ProjectileFull1").GetComponent<Image>().enabled = true;
                break;
            case 2:
                GameObject.Find("ProjectileFull3").GetComponent<Image>().enabled = false;
                GameObject.Find("ProjectileFull2").GetComponent<Image>().enabled = true;
                GameObject.Find("ProjectileFull1").GetComponent<Image>().enabled = true;
                break;
            case 1:
                GameObject.Find("ProjectileFull3").GetComponent<Image>().enabled = false;
                GameObject.Find("ProjectileFull2").GetComponent<Image>().enabled = false;
                GameObject.Find("ProjectileFull1").GetComponent<Image>().enabled = true;
                break;
            case 0:
                GameObject.Find("ProjectileFull3").GetComponent<Image>().enabled = false;
                GameObject.Find("ProjectileFull2").GetComponent<Image>().enabled = false;
                GameObject.Find("ProjectileFull1").GetComponent<Image>().enabled = false;
                break;
        }
    }

    public void UpdateCurrentHealthUI()
    {
        switch (CurrentHealth)
        {
            case 3:
                GameObject.Find("HeartFull3").GetComponent<Image>().enabled = true;
                GameObject.Find("HeartFull2").GetComponent<Image>().enabled = true;
                GameObject.Find("HeartFull1").GetComponent<Image>().enabled = true;
                break;
            case 2:
                GameObject.Find("HeartFull3").GetComponent<Image>().enabled = false;
                GameObject.Find("HeartFull2").GetComponent<Image>().enabled = true;
                GameObject.Find("HeartFull1").GetComponent<Image>().enabled = true;
                break;
            case 1:
                GameObject.Find("HeartFull3").GetComponent<Image>().enabled = false;
                GameObject.Find("HeartFull2").GetComponent<Image>().enabled = false;
                GameObject.Find("HeartFull1").GetComponent<Image>().enabled = true;
                break;
            case 0:
                GameObject.Find("HeartFull3").GetComponent<Image>().enabled = false;
                GameObject.Find("HeartFull2").GetComponent<Image>().enabled = false;
                GameObject.Find("HeartFull1").GetComponent<Image>().enabled = false;
                break;
        }
    }

    private void TriggerAction(string action, bool isCombatAction = true)
    {
        this.animator.SetTrigger(action);
        this.animator.SetBool("IsBlocking", this.isBlocking);

        if (isCombatAction)
        {
            this.animator.SetTrigger("TriggerCombatAction");
        }
    }

    public void ChangeDirection(Direction newDirection)
    {
        if (this.CurrentDirection == newDirection)
        {
            return;
        }

        Vector3 rotation = this.transform.localRotation.eulerAngles;
        rotation.y -= 180;
        this.transform.localEulerAngles = rotation;
        this.CurrentDirection = newDirection;

        SpriteRenderer[] sprites = this.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < sprites.Length; i++)
        {
            Vector3 position = sprites[i].transform.localPosition;
            position.z *= -1;
            sprites[i].transform.localPosition = position;
        }

        //Transform dialogueTranform = transform.Find("CharacterCanvas");
        //RectTransform dialogueTransform = GetComponent<RectTransform>();
        Transform dialogueCanvas = FindChildObjectWithTag(transform, "Dialogue");
        if (dialogueCanvas != null)
        {
            Vector3 dialogueRotation = dialogueCanvas.localRotation.eulerAngles;
            dialogueRotation.y -= 180;
            dialogueCanvas.localEulerAngles = rotation;
            //RectTransform dialogueTransform = dialogueTranform.GetComponent<RectTransform>();
            // if (dialogueTransform != null)
            // {
            //     Vector3 dialogueRotation = dialogueTransform.localRotation.eulerAngles;
            //     dialogueRotation.y -= 180;
            //     dialogueTransform.localEulerAngles = rotation;
            // }
        }
    }

    private Transform FindChildObjectWithTag(Transform parent, string tag)
    {
        foreach (Transform child in parent)
        {

        }
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.tag == tag)
            {
                return child;
            }
        }
        return null;
    }

    private void OnCastEffect()
    {
        if (this.Effect != null)
        {
            this.activeEffect = WeaponEffect.Create(this.Effect, this.EffectPoint);
        }
    }
    private void OnCastEffectStop()
    {
        if (this.activeEffect != null)
        {
            this.activeEffect.Stop();
            this.activeEffect = null;
        }
    }

    private void OnCastComplete()
    {
        this.OnCastEffectStop();

        this.LaunchProjectile(this.CastProjectile);
    }

    private void OnThrowMainComplete()
    {
        this.LaunchProjectile(this.ThrowMainProjectile);
    }

    private void OnThrowOffComplete()
    {
        this.LaunchProjectile(this.ThrowOffProjectile);
    }

    private void LaunchProjectile(WeaponProjectile projectile)
    {
        if (projectile != null)
        {
            WeaponProjectile.Create(
                projectile,
                this,
                this.LaunchPoint,
                (this.CurrentDirection == Direction.Left ? -1 : 1));
        }
    }

    private Collider2D CheckGround()
    {
        return Physics2D.OverlapCircle(GroundChecker.position, this.groundRadius, this.GroundLayer);
    }

    private void CreateSplash(Vector2 point)
    {
        if (this.SplashEmitter != null)
        {
            ParticleSystem splash = GameObject.Instantiate<ParticleSystem>(this.SplashEmitter);
            splash.transform.position = point;
            splash.Play();
            StartCoroutine(this.DestroyAfter(splash.duration, splash.gameObject));
        }
    }

    private void CreateDustCloud(Vector2 point)
    {
        if (this.DustEmitter != null)
        {
            ParticleSystem dust = GameObject.Instantiate<ParticleSystem>(this.DustEmitter);
            dust.transform.position = point;
            dust.Play();
            StartCoroutine(this.DestroyAfter(dust.duration, dust.gameObject));
        }
    }

    private bool IsTrigger(GameObject other, string name)
    {
        name = name.ToLower();

        if ((other.tag != null && other.tag.ToLower() == name) ||
            (other.name != null && other.name.ToLower() == name))
        {
            return true;
        }

        return false;
    }

    public bool IsAction(Action value, Action flag)
    {
        return (value & flag) != 0;
    }

    private float GetMultiplier(float factor)
    {
        if (Mathf.Sign(factor) < 0)
        {
            return 1 + factor;
        }
        else
        {
            return factor;
        }
    }

    private IEnumerator DestroyAfter(float seconds, GameObject gameObject)
    {
        yield return new WaitForSeconds(seconds);

        if (gameObject.tag == "Player")
        {
            Instantiate(RIPStone, transform.position, Quaternion.identity);
            StageManager.Instance.ShowMainMenuButton();
        }

        GameObject.Destroy(gameObject);
    }

    private IEnumerator EnableAfter(float seconds, Behaviour obj)
    {
        yield return new WaitForSeconds(seconds);

        obj.enabled = true;
    }
}