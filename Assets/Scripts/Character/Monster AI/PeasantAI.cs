﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeasantAI : CharacterAI
{
    public Transform PointLeft;
    public Transform PointRight;
    public Character.Direction StartPointDirection = Character.Direction.Left;
    public float AttackDistance = 0.7f;

    private Transform currentPoint;
    private Character.Direction pointDirection;

    [Header("Diaglog")]
    private ObjectManager[] objectManagers;
    private bool dialogStartTimer = false;
    private bool allowDialog = true;
    private float time;
    protected override void Start()
    {
        base.Start();
        character.ChangeDirection(Character.Direction.Left);

        objectManagers = GetComponentsInChildren<ObjectManager>();
    }

    void FixedUpdate()
    {
        if (!hasDetectedTarget)
        {
            StopMove();
            Detect();
        }

        if (hasDetectedTarget)
        {
            UpdateTargetInfo();
            CheckTraceRange();
            isMoveEnabled = true;
            RunAway();
            if (allowDialog)
            {
                foreach (ObjectManager objectManager in objectManagers)
                {
                    objectManager.AllowFadeOutDisPlay = false;
                    objectManager.AllowFadeInDisplay = true;
                }
                dialogStartTimer = true;
            }
        }

        if (allowDialog)
        {
            if (dialogStartTimer)
            {
                time += Time.deltaTime;
            }

            if (time > 5)
            {
                foreach (ObjectManager objectManager in objectManagers)
                {
                    objectManager.AllowFadeOutDisPlay = true;
                    objectManager.AllowFadeInDisplay = false;
                }
                dialogStartTimer = false;
                allowDialog = false;
            }
        }
    }

    private void RunAway()
    {
        if (isMoveEnabled)
        {
            if (detectedTargetDirection == Character.Direction.Left)
            {
                MoveRight();
            }
            else if (detectedTargetDirection == Character.Direction.Right)
            {
                MoveLeft();
            }
        }
    }
}
