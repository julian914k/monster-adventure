﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyclopsAI : CharacterAI
{
    public Transform PointLeft;
    public Transform PointRight;
    public Character.Direction StartPointDirection = Character.Direction.Left;
    public float AttackDistance = 0.7f;

    private Transform currentPoint;
    private Character.Direction pointDirection;

    protected override void Start()
    {
        base.Start();

        pointDirection = StartPointDirection;
        if (pointDirection == Character.Direction.Left)
            currentPoint = PointLeft;
        else
            currentPoint = PointRight;

        action = Character.Action.QuickAttack;
    }

    void FixedUpdate()
    {
        if (!isAttackEnabled)
        {
            if (attackCoolTime <= 0)
            {
                isAttackEnabled = true;
            }
            else
            {
                attackCoolTime -= Time.deltaTime;
            }
        }

        PointLeft.transform.position = new Vector2(PointLeft.transform.position.x, transform.position.y + 0.5f);
        PointRight.transform.position = new Vector2(PointRight.transform.position.x, transform.position.y + 0.5f);

        if (!hasDetectedTarget)
        {
            Detect();
            Move();
        }

        if (hasDetectedTarget)
        {
            UpdateTargetInfo();
            CheckTraceRange();

            if (isAttackEnabled)
            {
                if (Mathf.Abs(detectedTargetDistance.x) < AttackDistance)
                {
                    StopMove();
                    character.Perform(action);
                    attackCoolTime = character.MeleeAttackCoolTime;
                    isAttackEnabled = false;
                }
                else
                {

                    Trace();
                }
            }
        }
    }

    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (Object.ReferenceEquals(collider2D.gameObject, PointLeft.gameObject) || Object.ReferenceEquals(collider2D.gameObject, PointRight.gameObject))
        {
            CheckPointDirection();
        }
    }

    private void CheckPointDirection()
    {
        float distanceX = transform.position.x - currentPoint.position.x;
        if (distanceX > 0)
        {
            pointDirection = Character.Direction.Right;
            currentPoint = PointRight;
        }
        else
        {
            pointDirection = Character.Direction.Left;
            currentPoint = PointLeft;
        }
    }

    private void Move()
    {
        if (pointDirection == Character.Direction.Left)
        {
            MoveLeft();
        }
        else if (pointDirection == Character.Direction.Right)
        {
            MoveRight();
        }
    }
}
