﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightAI : CharacterAI
{
    public Transform PointLeft;
    public Transform PointRight;
    public Character.Direction StartPointDirection = Character.Direction.Left;
    public float AttackDistance = 0.7f;

    private Transform currentPoint;
    private Character.Direction pointDirection;

    protected override void Start()
    {
        base.Start();
        action = Character.Action.QuickAttack;
    }

    void FixedUpdate()
    {
        if (!isAttackEnabled)
        {
            if (attackCoolTime <= 0)
            {
                isAttackEnabled = true;
            }
            else
            {
                attackCoolTime -= Time.deltaTime;
            }
        }
        PointLeft.transform.position = new Vector2(PointLeft.transform.position.x, transform.position.y + 0.5f);
        PointRight.transform.position = new Vector2(PointRight.transform.position.x, transform.position.y + 0.5f);

        if (!hasDetectedTarget)
        {
            Detect();
        }

        if (hasDetectedTarget)
        {
            UpdateTargetInfo();
            CheckTraceRange();

            if (hasDetectedTarget && isAttackEnabled)
            {
                if (Mathf.Abs(detectedTargetDistance.x) < AttackDistance)
                {
                    StopMove();
                    character.Perform(action);
                    attackCoolTime = character.MeleeAttackCoolTime;
                    isAttackEnabled = false;
                }
                else
                {
                    Trace();
                }
            }
        }
    }

    void Update()
    {

    }
}
