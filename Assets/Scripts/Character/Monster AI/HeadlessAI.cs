﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadlessAI : CharacterAI
{
    public Transform PointLeft;
    public Transform PointRight;
    public Character.Direction StartPointDirection = Character.Direction.Left;
    public float AttackDistance = 0.7f;

    private Transform currentPoint;
    private Character.Direction pointDirection;
    private bool ableAttack = false;

    protected override void Start()
    {
        base.Start();

        pointDirection = StartPointDirection;
        if (pointDirection == Character.Direction.Left)
            currentPoint = PointLeft;
        else
            currentPoint = PointRight;

        action = Character.Action.Attack;
    }

    void FixedUpdate()
    {
        if (!hasDetectedTarget)
        {
            Detect();
            Move();
        }

        if (hasDetectedTarget)
        {
            UpdateTargetInfo();
            CheckTraceRange();
            Trace();
            if (Mathf.Abs(detectedTargetDistance.x) < AttackDistance)
            {
                StopMove();
                character.Perform(action);
            }
            else
            {

            }
        }
    }

    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D.tag == "TurnPoint")
            CheckPointDirection();
    }

    private void CheckPointDirection()
    {
        float distanceX = transform.position.x - currentPoint.position.x;
        if (distanceX > 0)
        {
            pointDirection = Character.Direction.Right;
            currentPoint = PointRight;
        }
        else
        {
            pointDirection = Character.Direction.Left;
            currentPoint = PointLeft;
        }
    }

    protected override void Trace()
    {
        if (AllowMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, detectedTarget.position, character.RunSpeed * Time.deltaTime * 0.015f);
        }
    }

    private void Move()
    {
        if (pointDirection == Character.Direction.Left)
        {
            MoveLeft();
        }
        else if (pointDirection == Character.Direction.Right)
        {
            MoveRight();
        }
    }
}
