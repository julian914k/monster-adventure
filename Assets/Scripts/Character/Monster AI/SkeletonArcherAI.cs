﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Character))]
public class SkeletonArcherAI : MonoBehaviour
{
    private Character character;
    private Vector2 axis;

    void Start()
    {
        this.character = this.GetComponent<Character>();
        this.axis = new Vector2();

        StartCoroutine(Action(2.0f, Character.Action.ThrowOff));
    }

    protected IEnumerator Action(float delayTime, Character.Action action)
    {
        while (!character.IsDead)
        {
            yield return new WaitForSeconds(delayTime);

            Character.Action actionFlags = 0;
            
            actionFlags |= ((action == Character.Action.QuickAttack) ? Character.Action.QuickAttack : 0);
            actionFlags |= ((action == Character.Action.Attack) ? Character.Action.Attack : 0);
            actionFlags |= ((action == Character.Action.ThrowOff) ? Character.Action.ThrowOff : 0);
			actionFlags |= ((action == Character.Action.ThromMain) ? Character.Action.ThromMain : 0);
            actionFlags |= ((action == Character.Action.Cast) ? Character.Action.Cast : 0);
            character.Perform(actionFlags);
        }
    }
    void OnTriggerEnter2D(Collider2D c)
    {
        if ((c.tag != null && c.tag == "MainItem") ||
            (c.name != null && c.name == "MainItem"))
        {
            Character hurtBy = c.GetComponent<Character>();
            if (hurtBy != null && hurtBy.IsAttacking)
            {
                float direction = c.transform.position.x - this.transform.position.x;
                this.character.ApplyDamage(1, direction);
            }
        }
    }
}
