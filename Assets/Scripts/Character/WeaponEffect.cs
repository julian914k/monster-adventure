﻿using UnityEngine;
using System.Collections;

public abstract class WeaponEffect : MonoBehaviour
{
    private static Color? mainBackgroundColor;
    private static int backgroundColorChangers;
    protected Transform target { get; private set; }

    public static WeaponEffect Create(WeaponEffect instance, Transform target)
    {
        WeaponEffect effect = GameObject.Instantiate<WeaponEffect>(instance);
        effect.target = target;

        return effect;
    }

    public static void SetBackgroundColor(Color newColor)
    {
        WeaponEffect.backgroundColorChangers++;

        Camera.main.backgroundColor = newColor;
    }

    private static void ResetBackgroundColor()
    {
        WeaponEffect.backgroundColorChangers--;

        if (WeaponEffect.backgroundColorChangers <= 0 && WeaponEffect.mainBackgroundColor.HasValue)
        {
            Camera.main.backgroundColor = WeaponEffect.mainBackgroundColor.GetValueOrDefault();
        }
    }

    void Awake()
    {
        if (!WeaponEffect.mainBackgroundColor.HasValue)
        {
            WeaponEffect.mainBackgroundColor = Camera.main.backgroundColor;
        }
    }

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
    }

    public virtual void Stop()
    {
        GameObject.Destroy(this.gameObject);

        WeaponEffect.ResetBackgroundColor();
    }
}