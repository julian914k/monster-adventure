﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
public abstract class CharacterAI : MonoBehaviour
{
    [Header("Detect")]
    public bool UseRaycast = true;
    public bool UseOverlapCircle = false;
    public Transform DetectPoint;
    public LayerMask DetectedTargetLayer;
    public float DetectRange = 3.0f;
    public Vector2 TraceRange = new Vector2(5.0f, 2.0f);

    [Header("AI Properties")]
    public float DelayActionTime = 2;
    public bool AllowFly = false;
    public bool AllowMove = true;

    protected Character character;
    protected Character.Action action;
    protected Vector2 axis;
    protected bool hasDetectedTarget = false;
    protected Transform detectedTarget;
    protected Character.Direction detectedTargetDirection;
    protected Vector2 detectedTargetDistance;
    protected bool isAttackEnabled = false;
    protected bool isMoveEnabled = true;
    protected float attackCoolTime = 0f;

    protected virtual void Start()
    {
        character = GetComponent<Character>();
    }

    protected IEnumerator Action(float delayTime, Character.Action action)
    {
        while (!character.IsDead)
        {
            yield return new WaitForSeconds(delayTime);

            Character.Action actionFlags = 0;

            actionFlags |= ((action == Character.Action.Attack) ? Character.Action.Attack : 0);
            actionFlags |= ((action == Character.Action.QuickAttack) ? Character.Action.QuickAttack : 0);
            actionFlags |= ((action == Character.Action.Cast) ? Character.Action.Cast : 0);

            character.Perform(actionFlags);
        }
    }

    private void SetDetectedTargetDistance()
    {
        if (detectedTarget != null)
        {
            detectedTargetDistance.x = transform.position.x - detectedTarget.position.x;
            detectedTargetDistance.y = transform.position.y - detectedTarget.position.y;
        }
    }

    private void SetDetectedTargetDirection()
    {
        if (detectedTargetDistance.x >= 0)
            detectedTargetDirection = Character.Direction.Left;
        else
            detectedTargetDirection = Character.Direction.Right;
    }

    private void ReleaseDetectedTarget()
    {
        StopMove();
        detectedTarget = null;
        hasDetectedTarget = false;
    }

    protected void Detect()
    {
        if (UseRaycast)
        {
            DetectRaycast();
        }

        if (UseOverlapCircle)
        {
            DetectCircle();
        }
    }

    private void DetectRaycast()
    {
        bool isLeftDir = (character.CurrentDirection == Character.Direction.Left);

        Vector2 endPos = new Vector2(DetectPoint.position.x - (isLeftDir ? DetectRange : -DetectRange), DetectPoint.position.y);

        Debug.DrawLine(DetectPoint.position, endPos, Color.green);

        RaycastHit2D hitInfo = Physics2D.Linecast(DetectPoint.position, endPos, DetectedTargetLayer);

        if (hitInfo.collider != null)
        {
            detectedTarget = hitInfo.collider.transform;
            hasDetectedTarget = true;
        }
    }

    private void DetectCircle()
    {
        Collider2D detectedTargetCollider = Physics2D.OverlapCircle(DetectPoint.position, DetectRange, DetectedTargetLayer);
        if (detectedTargetCollider != null)
        {
            detectedTarget = detectedTargetCollider.transform;
            hasDetectedTarget = true;
        }
    }

    protected void UpdateTargetInfo()
    {
        SetDetectedTargetDistance();
        SetDetectedTargetDirection();
    }

    protected void CheckTraceRange()
    {
        if (UseRaycast)
        {
            if (Mathf.Abs(detectedTargetDistance.x) - TraceRange.x > 0)
            {
                ReleaseDetectedTarget();
            }
        }

        if (UseOverlapCircle)
        {
            if (Mathf.Abs(detectedTargetDistance.x) - TraceRange.x > 0 ||
                Mathf.Abs(detectedTargetDistance.y) - TraceRange.y > 0)
            {
                ReleaseDetectedTarget();
            }
        }
    }

    protected virtual void Trace()
    {
        isMoveEnabled = true;

        if (AllowMove)
        {
            if (detectedTargetDirection == Character.Direction.Left)
            {
                MoveLeft();
            }
            else if (detectedTargetDirection == Character.Direction.Right)
            {
                MoveRight();
            }
        }
    }

    protected void MoveLeft()
    {
        if (isMoveEnabled)
        {
            axis.x = -1;
            axis.y = 0;
            character.Move(axis, true);
        }
    }

    protected void MoveRight()
    {
        if (isMoveEnabled)
        {
            axis.x = 1;
            axis.y = 0;
            character.Move(axis, true);
        }
    }

    protected void StopMove()
    {
        isMoveEnabled = false;
        character.GetRigidbody2D().velocity = Vector2.zero;
        axis.x = 0;
        axis.y = 0;
        character.Move(axis, true);
    }
}
