﻿using UnityEngine;
using System.Collections;

public class HorizontalProjectile : WeaponProjectile
{
    [Header("Projectile")]
    public int Speed = 500;
    public int RotationSpeed = 0;

    protected Transform RenderTransform { get; private set; }
    protected Vector3 RotationPoint = new Vector3();


    protected override void Start()
    {
        base.Start();

        SpriteRenderer sprite = this.GetComponentInChildren<SpriteRenderer>();
        if (sprite != null)
        {
            this.RenderTransform = sprite.transform;
        }

        SliderJoint2D joint = this.GetComponent<SliderJoint2D>();
        if (joint != null)
        {
            joint.anchor = new Vector2(this.transform.position.x, -this.transform.position.y);
        }

        float x = DirectionX * this.Speed;
        Rigidbody2D body = this.GetComponent<Rigidbody2D>();
        if (body != null)
        {
            body.AddForce(new Vector2(x, 0));
        }
    }

    protected override void Update()
    {
        base.Update();

        if (this.RotationSpeed != 0 && this.RenderTransform != null && !this.isStopped)
        {
            this.RenderTransform.RotateAround(this.transform.position + this.RotationPoint, Vector3.forward, Time.deltaTime * -this.RotationSpeed * this.DirectionX);
        }
    }
}