﻿using UnityEngine;
using System.Collections;
using CnControls;

[RequireComponent(typeof(Character))]
public class CharacterInput : MonoBehaviour
{
    private Character character;
    private Vector2 axis;
    private bool isAttackEnabled = false;
    private float attackCoolTime = 0f;

    void Start()
    {
        this.character = this.GetComponent<Character>();
        this.axis = new Vector2();
    }

    void FixedUpdate()
    {
        if (!isAttackEnabled)
        {
            if (attackCoolTime <= 0)
            {
                isAttackEnabled = true;
            }
            else
            {
                attackCoolTime -= Time.deltaTime;
            }
        }

        this.axis.x = CnInputManager.GetAxis("Horizontal");
        this.axis.y = CnInputManager.GetAxis("Vertical");

        bool isHorizontalStillPressed = CnInputManager.GetAxisRaw("Horizontal") != 0;

        this.character.Move(this.axis, isHorizontalStillPressed);
    }

    void Update()
    {
        Character.Action actionFlags = 0;

        actionFlags |= (CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.RunModified : 0);
        actionFlags |= (CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.Crouch : 0);
        actionFlags |= (CnInputManager.GetButtonDown("Jump") ? Character.Action.Jump : 0);
        actionFlags |= (CnInputManager.GetButtonDown("Jump") && CnInputManager.GetAxisRaw("Vertical") < 0 ? Character.Action.JumpDown : 0);

        if (isAttackEnabled)
        {
            if (character.CurrentProjectileCount > 0 && character.IsPlayer)
            {
                actionFlags |= (CnInputManager.GetButtonDown("Fire2") ? Character.Action.Cast : 0);
                if (character.IsAction(actionFlags, Character.Action.Cast))
                {
                    attackCoolTime = character.RangedAttackCoolTime;
                    isAttackEnabled = false;
                }
            }
            actionFlags |= (CnInputManager.GetButtonDown("Fire1") ? Character.Action.QuickAttack : 0);
            if (character.IsAction(actionFlags, Character.Action.QuickAttack))
            {
                attackCoolTime = character.MeleeAttackCoolTime;
                isAttackEnabled = false;
            }

            if (CnInputManager.GetButtonDown("Fire3"))
            {
                Debug.Log("FIre3 콜");
                CharacterManager.Instance.SwapCharacter();
            }
        }

        this.character.Perform(actionFlags);
    }
}
