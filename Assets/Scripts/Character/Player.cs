﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private static Character instance;

    public static Character GetInstance(){
        if(instance == null){
            instance = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Character>();
        }

        return instance;
    }
}
