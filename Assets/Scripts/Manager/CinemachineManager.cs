﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineManager : MonoBehaviour
{
    public bool movePlayer = true;
    private ObjectManager[] objectManagers;
    private float time;
    private bool startTimer;
    private SpriteRenderer graveTile;

    void Start()
    {
        if (movePlayer)
        {
            Player.GetInstance().GetRigidbody2D().isKinematic = true;
            Player.GetInstance().GetComponent<CharacterInput>().enabled = false;

            graveTile = GameObject.Find("GraveTile").GetComponent<SpriteRenderer>();
            if (graveTile != null)
            {
                graveTile.sortingOrder = 500;
            }
        }

        objectManagers = Player.GetInstance().GetComponentsInChildren<ObjectManager>();
    }

    void FixedUpdate()
    {
        if (movePlayer)
        {
            Vector3 v3 = new Vector3(Player.GetInstance().transform.position.x, Player.GetInstance().transform.position.y + Time.deltaTime);
            Player.GetInstance().transform.position = v3;

            if (Player.GetInstance().transform.position.y > -3.2f)
            {
                movePlayer = false;
                Player.GetInstance().GetRigidbody2D().isKinematic = false;
                Player.GetInstance().GetComponent<CharacterInput>().enabled = true;

                if (graveTile != null)
                {
                    graveTile.sortingOrder = 0;
                }

                foreach (ObjectManager objectManager in objectManagers)
                {
                    objectManager.AllowFadeOutDisPlay = false;
                    objectManager.AllowFadeInDisplay = true;
                }
                startTimer = true;
            }
        }

        if (startTimer)
        {
            time += Time.deltaTime;
        }

        if (time > 5)
        {
            foreach (ObjectManager objectManager in objectManagers)
            {
                objectManager.AllowFadeOutDisPlay = true;
                objectManager.AllowFadeInDisplay = false;
            }
            startTimer = false;
        }
    }
}
