﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;
using BayatGames.SaveGameFree;

public class StageManager : MonoBehaviour
{
    public static StageManager Instance = null;
    public Text CoinText;
    private int coinScore = 0;
    public GameObject MainMenuButton;
    public GameObject StageClear;
    public PlayableDirector director1;
    public GameObject EscapeQuestionBox;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            // 16대9 비율로 스크린 사이즈 조정
            UtilityManager.SetScreenResolution(16, 9);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void Start()
    {
        coinScore = LoadCoin();
        CoinText.text = coinScore.ToString();
    }
    private void Update()
    {
        // if (Application.platform == RuntimePlatform.Android)        
        if (Input.GetKey(KeyCode.Escape))
        {
            EscapeQuestionBox.SetActive(true);
        }
    }
    public void AddCoinScore()
    {
        coinScore++;
        CoinText.text = coinScore.ToString();
    }
    public void LoadGameOver()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LoadStageSelection()
    {
        SceneManager.LoadScene("StageSelection");
    }

    public void EndStage()
    {
        if (director1 != null)
        {
            director1.enabled = true;
        }
        else
        {
            ShowResult();
        }
    }

    public void ShowResult()
    {
        StageClear.GetComponent<Image>().enabled = true;
        StageClear.GetComponent<Button>().enabled = true;
        StageClear.GetComponentInChildren<Text>().enabled = true;
    }

    public void ShowMainMenuButton()
    {
        StageClear.GetComponent<Image>().enabled = false;
        StageClear.GetComponent<Button>().enabled = false;
        StageClear.GetComponentInChildren<Text>().enabled = false;
        MainMenuButton.SetActive(true);
        //MainMenuButton.GetComponentInChildren<Image>().enabled = true;
        //MainMenuButton.GetComponentInChildren<Button>().enabled = true;
        //MainMenuButton.GetComponentInChildren<Text>().enabled = true;
    }
    public void SaveCoin()
    {
        SaveGame.Save<int>("Coin", Convert.ToInt32(CoinText.text));
    }
    public int LoadCoin()
    {
        return SaveGame.Load<int>("Coin");
    }
    public void HideQuestionBox()
    {
        EscapeQuestionBox.SetActive(false);
    }
}
