﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject MainCamera;
    public float ParallaxFactor;
    private float startPositionX;
    private float length;

    void Start()
    {
        startPositionX = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {        
        float temp = MainCamera.transform.position.x * ParallaxFactor;
        float dist = MainCamera.transform.position.x * (1 - ParallaxFactor);

        transform.position = new Vector3(startPositionX + dist, transform.position.y, transform.position.z);

        if (temp > startPositionX + length)
        {
            startPositionX += length;
        }
        else if (temp < startPositionX - length)
        {
            startPositionX -= length;
        }
    }
}
