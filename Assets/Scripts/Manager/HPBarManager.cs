﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarManager : MonoBehaviour
{
    public Image CurBar;
    private Character character;

    void Start()
    {
        character = transform.root.GetComponentInChildren<Character>();
    }

    public void UpdateHPBar()
    {
        CurBar.fillAmount = ((float)character.CurrentHealth / (float)character.MaxHealth);
        ShowHPBar();
        Invoke("HideHPBar", 1f);
        if (character.CurrentHealth != 0)
        {
            ShowHPBar();
            Invoke("HideHPBar", 1f);
        }
    }

    public void ShowHPBar()
    {
        Image[] HPBarImages = transform.GetComponentsInChildren<Image>();
        foreach (Image HPBarImage in HPBarImages)
        {
            HPBarImage.enabled = true;
        }
    }

    public void HideHPBar()
    {
        Image[] HPBarImages = gameObject.GetComponentsInChildren<Image>();
        foreach (Image HPBarImage in HPBarImages)
        {
            HPBarImage.enabled = false;
        }
    }

    public void Destroy(float destroyDelayTime)
    {
        GameObject.Destroy(gameObject, destroyDelayTime);
    }

}
