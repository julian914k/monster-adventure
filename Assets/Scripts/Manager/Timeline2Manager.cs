﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Timeline2Manager : MonoBehaviour
{
    public bool isStarted = false;
    private bool rockStartedKey = false;
    public PlayableDirector PlayableDirector;
    public string state;

    private void Update()
    {
        state = PlayableDirector.state.ToString();
        if (!rockStartedKey)
        {
            if (PlayableDirector.state == PlayState.Playing)
            {
                isStarted = true;
                rockStartedKey = true;
            }
        }
        if (rockStartedKey)
            Debug.Log("PlayableDirector State: " + PlayableDirector.state);
        if (PlayableDirector.state != PlayState.Playing && isStarted)
        {
            Debug.Log("결과보여줌");
            Invoke("ShowResult", 2f);            
        }
    }

    private void ShowResult(){
        StageManager.Instance.ShowResult();
    }
}
