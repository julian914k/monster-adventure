﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    private bool fix = false;
    public Animator PlayerAnimator;
    public PlayableDirector PlayableDirector;
    private RuntimeAnimatorController playerRuntimeAnimatorController;
    private void OnEnable()
    {
        playerRuntimeAnimatorController = PlayerAnimator.runtimeAnimatorController;
        PlayerAnimator.runtimeAnimatorController = null;
    }

    void Update()
    {
        if (!fix)
        {
            if (PlayableDirector.state != PlayState.Playing)
            {
                fix = true;
                PlayerAnimator.runtimeAnimatorController = playerRuntimeAnimatorController;
                Player.GetInstance().CurrentDirection = Character.Direction.Right;
            }
        }
    }
}
