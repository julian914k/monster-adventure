﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.LuisPedroFonseca.ProCamera2D;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager Instance = null;
    public int LeaderIndex = 1;
    [Header("Team Member Info")]
    public GameObject TeamMember1;
    public GameObject TeamMember2;
    public GameObject TeamMember3;
    public Image LeaderFace;
    public Sprite TeamFace1;
    public Sprite TeamFace2;
    public Sprite TeamFace3;
    public float TeamRunSpeed1;
    public float TeamRunSpeed2;
    public float TeamRunSpeed3;
    public float TeamJumpPower1;
    public float TeamJumpPower2;
    public float TeamJumpPower3;
    public float TeamMeleeAttackCoolTime1;
    public float TeamMeleeAttackCoolTime2;
    public float TeamMeleeAttackCoolTime3;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void SwapCharacter()
    {
        // 캐릭터 스왑을 위해 해야될 작업
        // 리더 index 변경
        if (LeaderIndex >= 3)
        {
            LeaderIndex = 1;
        }
        else
        {
            LeaderIndex++;
        }
        // 1. 캐릭터 리더(Input, TeamAI 스크립트 추가 제거)
        GameObject oldLeader = null;
        GameObject newLeader = null;
        float newLeaderRunSpeed = 0.0f;
        float newLeaderJumpPower = 0.0f;
        float newLeaderMeeleAttackCoolTime = 0.0f;
        // 변경된 스크립트 값 셋팅
        // 리더 인덱스 기준으로 TeamAI 초기값 셋팅 변경(Target Character, Follow Range, Jump Delay Time)

        switch (LeaderIndex)
        {
            case 1:
                // 기존 1번 캐릭터를 리더로 만들고, 기존 3번은 팀원이 된다.
                oldLeader = TeamMember3;
                newLeader = TeamMember1;
                SwitchLeaderScript(oldLeader, newLeader);
                TeamMember2.GetComponent<TeamAI>().FollowRange = 1;
                TeamMember3.GetComponent<TeamAI>().FollowRange = 2;
                TeamMember2.GetComponent<TeamAI>().RespawnRange.x = 10;
                TeamMember2.GetComponent<TeamAI>().RespawnRange.y = 10;
                TeamMember3.GetComponent<TeamAI>().RespawnRange.x = 10;
                TeamMember3.GetComponent<TeamAI>().RespawnRange.y = 10;
                TeamMember2.GetComponent<TeamAI>().JumpDelayTime = 0.5f;
                TeamMember3.GetComponent<TeamAI>().JumpDelayTime = 1.0f;
                TeamMember2.GetComponent<TeamAI>().TargetCharacter = newLeader.GetComponent<Character>();
                TeamMember3.GetComponent<TeamAI>().TargetCharacter = newLeader.GetComponent<Character>();

                // 변경된 리더의 이동속도로 나머지 동료들의 이동속도 동기화
                newLeaderRunSpeed = TeamRunSpeed1;
                TeamMember1.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;
                TeamMember2.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;
                TeamMember3.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;

                // 변경된 리더의 점프파워로 나머지 동료들의 점프파워 동기화
                newLeaderJumpPower = TeamJumpPower1;
                TeamMember1.GetComponent<Character>().JumpPower = newLeaderJumpPower;
                TeamMember2.GetComponent<Character>().JumpPower = newLeaderJumpPower;
                TeamMember3.GetComponent<Character>().JumpPower = newLeaderJumpPower;

                // 변경된 리더의 얼굴로 리더 얼굴 UI 변경
                LeaderFace.sprite = TeamFace1;

                // 변경된 리더의 공격 쿨타임으로 동기화
                newLeaderMeeleAttackCoolTime = TeamMeleeAttackCoolTime1;
                TeamMember1.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                TeamMember2.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                TeamMember3.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                break;
            case 2:
                oldLeader = TeamMember1;
                newLeader = TeamMember2;
                SwitchLeaderScript(oldLeader, newLeader);
                TeamMember3.GetComponent<TeamAI>().FollowRange = 1;
                TeamMember1.GetComponent<TeamAI>().FollowRange = 2;
                TeamMember3.GetComponent<TeamAI>().RespawnRange.x = 10;
                TeamMember3.GetComponent<TeamAI>().RespawnRange.y = 10;
                TeamMember1.GetComponent<TeamAI>().RespawnRange.x = 10;
                TeamMember1.GetComponent<TeamAI>().RespawnRange.y = 10;
                TeamMember3.GetComponent<TeamAI>().JumpDelayTime = 0.5f;
                TeamMember1.GetComponent<TeamAI>().JumpDelayTime = 1.0f;
                TeamMember3.GetComponent<TeamAI>().TargetCharacter = newLeader.GetComponent<Character>();
                TeamMember1.GetComponent<TeamAI>().TargetCharacter = newLeader.GetComponent<Character>();

                // 변경된 리더의 이동속도로 나머지 동료들의 이동속도 동기화
                newLeaderRunSpeed = TeamRunSpeed2;
                TeamMember1.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;
                TeamMember2.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;
                TeamMember3.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;

                // 변경된 리더의 점프파워로 나머지 동료들의 점프파워 동기화
                newLeaderJumpPower = TeamJumpPower2;
                TeamMember1.GetComponent<Character>().JumpPower = newLeaderJumpPower;
                TeamMember2.GetComponent<Character>().JumpPower = newLeaderJumpPower;
                TeamMember3.GetComponent<Character>().JumpPower = newLeaderJumpPower;

                // 변경된 리더의 얼굴로 리더 얼굴 UI 변경
                LeaderFace.sprite = TeamFace2;

                // 변경된 리더의 공격 쿨타임으로 동기화
                newLeaderMeeleAttackCoolTime = TeamMeleeAttackCoolTime2;
                TeamMember1.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                TeamMember2.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                TeamMember3.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                break;
            case 3:
                oldLeader = TeamMember2;
                newLeader = TeamMember3;
                SwitchLeaderScript(oldLeader, newLeader);
                TeamMember1.GetComponent<TeamAI>().FollowRange = 1;
                TeamMember2.GetComponent<TeamAI>().FollowRange = 2;
                TeamMember1.GetComponent<TeamAI>().RespawnRange.x = 10;
                TeamMember1.GetComponent<TeamAI>().RespawnRange.y = 10;
                TeamMember2.GetComponent<TeamAI>().RespawnRange.x = 10;
                TeamMember2.GetComponent<TeamAI>().RespawnRange.y = 10;
                TeamMember1.GetComponent<TeamAI>().JumpDelayTime = 0.5f;
                TeamMember2.GetComponent<TeamAI>().JumpDelayTime = 1.0f;
                TeamMember1.GetComponent<TeamAI>().TargetCharacter = newLeader.GetComponent<Character>();
                TeamMember2.GetComponent<TeamAI>().TargetCharacter = newLeader.GetComponent<Character>();

                // 변경된 리더의 이동속도로 나머지 동료들의 이동속도 동기화
                newLeaderRunSpeed = TeamRunSpeed3;
                TeamMember1.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;
                TeamMember2.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;
                TeamMember3.GetComponent<Character>().RunSpeed = newLeaderRunSpeed;

                // 변경된 리더의 점프파워로 나머지 동료들의 점프파워 동기화
                newLeaderJumpPower = TeamJumpPower3;
                TeamMember1.GetComponent<Character>().JumpPower = newLeaderJumpPower;
                TeamMember2.GetComponent<Character>().JumpPower = newLeaderJumpPower;
                TeamMember3.GetComponent<Character>().JumpPower = newLeaderJumpPower;

                // 변경된 리더의 얼굴로 리더 얼굴 UI 변경
                LeaderFace.sprite = TeamFace3;

                // 변경된 리더의 공격 쿨타임으로 동기화
                newLeaderMeeleAttackCoolTime = TeamMeleeAttackCoolTime3;
                TeamMember1.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                TeamMember2.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                TeamMember3.GetComponent<Character>().MeleeAttackCoolTime = newLeaderMeeleAttackCoolTime;
                break;
        }

        // 2. 캐릭터 위치(transform.position.x 값 +- 증감을 통해 조절)

        // 3. 캐릭터 태그
        // 태그 변경
        oldLeader.tag = "Team";
        newLeader.tag = "Player";
        // 4. 캐릭터 레이어

        // 아래와 같이 하면, 최상위 오브젝트의 레이어만 바뀌고, 자식 레이어는 바뀌지 않는 문제가 생김.
        // 자식 오브젝트의 레이어도 변경이 필요하다면, foreach를 통해 자식을 찾아서 각각 다 변경할 레이어를 할당해줘야함.
        oldLeader.layer = LayerMask.NameToLayer("Team");
        newLeader.layer = LayerMask.NameToLayer("Player");
        // 5. 캐릭터 능력
        // 캐릭터가 1명일 때, 2명일 때, 3명일 때 모두 고려되야함

        // 6. 카메라 팔로우 대상 변경
        // 프로 카메라 2D를 사용중이기에, 프로 카메라 2D 컴포넌트에서 팔로우 타겟 값을 변경할 방법을 찾아야함.
        ProCamera2D.Instance.RemoveAllCameraTargets();
        ProCamera2D.Instance.AddCameraTarget(newLeader.transform);
    }
    private void SwitchLeaderScript(GameObject oldLeader, GameObject newLeader)
    {
        newLeader.AddComponent<CharacterInput>();
        newLeader.AddComponent<Player>();
        oldLeader.AddComponent<TeamAI>();

        // AddComponent로 반복되서 추가되는만큼 비활성화가 아닌 삭제 명령어(Destroy) 등을 사용해 처리해야한다. 안그러면 계속 증식함.
        Destroy(oldLeader.GetComponent<CharacterInput>());
        Destroy(oldLeader.GetComponent<Player>());
        Destroy(newLeader.GetComponent<TeamAI>());
    }
}
