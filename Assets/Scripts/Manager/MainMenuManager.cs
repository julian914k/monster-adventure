﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using BayatGames.SaveGameFree;

public class MainMenuManager : MonoBehaviour
{
    public Text CoinText;
    public GameObject EscapeQuestionBox;
    private void Awake()
    {
        UtilityManager.SetScreenResolution(16, 9);
    }
    private void Start()
    {
        CoinText.text = SaveGame.Load<int>("Coin").ToString();
    }
    private void Update()
    {
        // if (Application.platform == RuntimePlatform.Android)        
        if (Input.GetKey(KeyCode.Escape))
        {
            EscapeQuestionBox.SetActive(true);
        }
    }
    public void QuitThisApp()
    {
        Application.Quit();
    }
    public void HideQuestionBox(){
        EscapeQuestionBox.SetActive(false);
    }
    public void GameStart()
    {
        SceneManager.LoadScene("StageSelection");
    }

    public void AddCoin(int AmountOfCoins)
    {
        int currentCoin = SaveGame.Load<int>("Coin");
        currentCoin += AmountOfCoins;
        SaveGame.Save("Coin", currentCoin);
        CoinText.text = currentCoin.ToString();
    }
}
