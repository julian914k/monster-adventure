﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilityManager
{
    public static void SetScreenResolution(int widthRatio, int heightRatio)
    {
        // 16대9 비율로 스크린 사이즈 조정
        Screen.SetResolution(Screen.width, Screen.width * heightRatio / widthRatio, true);
    }
}
