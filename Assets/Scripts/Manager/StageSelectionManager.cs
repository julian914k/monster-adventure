﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageSelectionManager : MonoBehaviour
{
    public Image Stage1Square;
    public Image Stage2Square;
    public Image Stage3Square;

    public StageName SelectedStageName;
    public Text StageTitle;
    public enum StageName
    {
        Stage1,
        Stage2,
        Stage3
    }
    public GameObject EscapeQuestionBox;
    private void Awake()
    {
        UtilityManager.SetScreenResolution(16, 9);
    }
    public void Start()
    {
        SelectedStageName = StageName.Stage1;
        switch (SelectedStageName)
        {
            case StageName.Stage1:
                Stage1Square.enabled = true;
                break;
            case StageName.Stage2:
                Stage2Square.enabled = true;
                break;
            case StageName.Stage3:
                Stage3Square.enabled = true;
                break;
        }
    }
    private void Update()
    {
        // if (Application.platform == RuntimePlatform.Android)        
        if (Input.GetKey(KeyCode.Escape))
        {
            EscapeQuestionBox.SetActive(true);
        }
    }
    public void LoadMainMenu()
    {
        StageManager.Instance.LoadMainMenu();
    }
    public void HideQuestionBox()
    {
        EscapeQuestionBox.SetActive(false);
    }
    public void SetStage(string stageName)
    {
        SelectedStageName = (StageName)StageName.Parse(typeof(StageName), stageName);
        switch (SelectedStageName)
        {
            case StageName.Stage1:
                StageTitle.text = "묘지 - 좀비의 역습";
                Stage1Square.enabled = true;
                Stage2Square.enabled = false;
                Stage3Square.enabled = false;
                break;
            case StageName.Stage2:
                StageTitle.text = "광산 - 광산의 지배자";
                Stage1Square.enabled = false;
                Stage2Square.enabled = true;
                Stage3Square.enabled = false;
                break;
            case StageName.Stage3:
                StageTitle.text = "용암동굴 - 임프 마을";
                Stage1Square.enabled = false;
                Stage2Square.enabled = false;
                Stage3Square.enabled = true;
                break;
        }
    }
    public void StartStage()
    {
        SceneManager.LoadScene(SelectedStageName.ToString());
    }
}