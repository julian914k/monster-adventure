﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public bool UseDestroyTimer = false;
    public float DestroyDelayTime;

    public static Item Create(Item instance, Vector3 position)
    {
        Item item = GameObject.Instantiate<Item>(instance);
        item.transform.position = position;
        return item;
    }

    void Start()
    {
        if (UseDestroyTimer)
        {
            Destroy(DestroyDelayTime);
        }
    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D.CompareTag("Player"))
        {
            switch (gameObject.name)
            {
                case "PotionHP":
                    if (collider2D.GetComponent<Character>().CurrentHealth < collider2D.GetComponent<Character>().MaxHealth)
                    {
                        collider2D.GetComponent<Character>().CurrentHealth++;
                    }
                    collider2D.GetComponent<Character>().UpdateCurrentHealthUI();
                    break;
                case "PotionMP":
                    collider2D.GetComponent<Character>().CurrentProjectileCount = collider2D.GetComponent<Character>().MaxProjectileCount;
                    collider2D.GetComponent<Character>().UpdateCurrentProjectileCountUI();
                    break;
                case "Coin":
                    StageManager.Instance.AddCoinScore();
                    break;
            }
            Destroy();
        }
    }

    private void Destroy()
    {
        GameObject.Destroy(gameObject);
    }

    private void Destroy(float destroyDelayTime)
    {
        if (UseDestroyTimer)
        {
            GameObject.Destroy(gameObject, destroyDelayTime);
        }
    }
}
