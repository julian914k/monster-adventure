﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectManager : MonoBehaviour
{
    public bool AllowFadeInDisplay = false;
    public bool AllowFadeOutDisPlay = false;
    public bool AllowDestroy = false;
    public bool AllowDestroyTimer = false;
    public float DestroyDelayTime;    
    public float MaxAlpha = 255.0f;
    public float Alpha = 255.0f;
    public float FadeInFactor = 1.0f;
    public float FadeOutFactor = 1.0f;

    private SpriteRenderer spriteRenderer;
    private MeshRenderer meshRenderer;
    private Image image;
    private Text text;
    private bool isSprite = false;
    private bool isMesh = false;
    private bool isImage = false;
    private bool isText = false;
    private float maxAlpha;
    public float alpha;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        image = GetComponent<Image>();
        text = GetComponent<Text>();

        if (spriteRenderer != null)
        {
            isSprite = true;
        }
        if (meshRenderer != null)
        {
            isMesh = true;
        }
        if (image != null)
        {
            isImage = true;
        }
        if (text != null)
        {
            isText = true;
        }

        if (AllowDestroyTimer)
        {
            Destroy(DestroyDelayTime);
        }
        
        maxAlpha = MaxAlpha/255;
        alpha = Alpha/255;
    }

    void FixedUpdate()
    {
        if (AllowFadeInDisplay)
        {
            if (isSprite)
                FadeInDisplay(spriteRenderer);
            if (isMesh)
                FadeInDisplay(meshRenderer);
            if (isImage)
                FadeInDisplay(image);
            if (isText)
                FadeInDisplay(text);
        }

        if (AllowFadeOutDisPlay)
        {
            if (isSprite)
                FadeOutDestroy(spriteRenderer);
            if (isMesh)
                FadeOutDestroy(meshRenderer);
            if (isImage)
                FadeOutDestroy(image);
            if (isText)
                FadeOutDestroy(text);
        }
    }

    private void FadeInDisplay(SpriteRenderer spriteRenderer)
    {
        if (alpha < maxAlpha)
        {
            Color color = spriteRenderer.color;
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            spriteRenderer.color = color;
        }
    }

    private void FadeInDisplay(MeshRenderer meshRenderer)
    {
        if (alpha < maxAlpha)
        {
            Color color = meshRenderer.material.GetColor("_Color");
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            meshRenderer.material.SetColor("_Color", color);
        }
    }

    private void FadeInDisplay(Image image)
    {
        if (alpha < maxAlpha)
        {
            Color color = image.color;
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            image.color = color;
        }
    }

    private void FadeInDisplay(Text text)
    {
        if (alpha < maxAlpha)
        {
            Color color = text.color;
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            text.color = color;
        }
    }

    private void FadeOutDestroy(SpriteRenderer spriteRenderer)
    {
        if (alpha > 0)
        {
            Color color = spriteRenderer.color;
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            spriteRenderer.color = color;
        }
        else if (AllowDestroy)
        {
            Destroy();
        }
    }

    private void FadeOutDestroy(MeshRenderer meshRenderer)
    {
        if (alpha > 0)
        {
            Color color = meshRenderer.material.GetColor("_Color");
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            meshRenderer.material.SetColor("_Color", color);
        }
        else if (AllowDestroy)
        {
            Destroy();
        }
    }

    private void FadeOutDestroy(Image image)
    {
        if (alpha > 0)
        {
            Color color = image.color;
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            image.color = color;
        }
        else if (AllowDestroy)
        {
            Destroy();
        }
    }

    private void FadeOutDestroy(Text text)
    {
        if (alpha > 0)
        {
            Color color = text.color;
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            text.color = color;
        }
        else if (AllowDestroy)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        GameObject.Destroy(gameObject);
    }

    private void Destroy(float destroyDelayTime)
    {
        if (AllowDestroyTimer)
        {
            GameObject.Destroy(gameObject, destroyDelayTime);
        }
    }
}
