﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPoint : MonoBehaviour
{
    [Tooltip("해당 튜토리얼 메시지들을 가지고 있는 상위 게임 오브젝트")]
    public GameObject TutorialMessage;

    private ObjectManager[] objectManagers;

    void Start()
    {
        objectManagers = TutorialMessage.GetComponentsInChildren<ObjectManager>();
    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D.tag == "Player")
        {
            foreach (ObjectManager objectManager in objectManagers)
            {
                objectManager.AllowFadeOutDisPlay = false;
                objectManager.AllowFadeInDisplay = true;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider2D)
    {
        if (collider2D.tag == "Player")
        {
            foreach (ObjectManager objectManager in objectManagers)
            {
                objectManager.AllowFadeInDisplay = false;
                objectManager.AllowFadeOutDisPlay = true;
            }
        }
    }
}
