﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class StageClearArea : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {            
            StageManager.Instance.SaveCoin();
            Player.GetInstance().GetComponent<CharacterInput>().enabled = false;
            Player.GetInstance().GetRigidbody2D().velocity = Vector2.zero;
            StageManager.Instance.EndStage();
        }
    }
}
